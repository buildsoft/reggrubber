﻿using Microsoft.EntityFrameworkCore;
using RegGrubber.Enums;
using RegGrubber.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RegGrubber.ReginaldDbContext;
using CustomerMaintenance = RegGrubber.Models.CustomerMaintenance;

namespace RegGrubber.Data
{
    public static class CustomerMaintenanceRepository
    {
        private static readonly ReginaldContext Context;

        static CustomerMaintenanceRepository()
        {
            Context = new ReginaldContext();
        }

        public static async Task<List<CustomerMaintenance>> GetCustomerMaintenancesAsync(IEnumerable<ProductEditionType> productTypes)
        {
            var cubiEnterpriseProductTypes = ProductEditionTypeExtensions.GetEnterpriseCubitProductTypes(typeof(ProductEditionType)).ToArray();
            var allCubitProductTypes = ProductEditionTypeExtensions.GetAllCubitProductTypes(typeof(ProductEditionType)).ToArray();

            var maintenanceCustomers = await (from customer in Context.Customer
                join cm in Context.CustomerMaintenance on customer.Id equals cm.CustomerId
                join cmp in Context.CustomerMaintenanceProduct on cm.Id equals cmp.MaintenanceId
                where productTypes.Contains((ProductEditionType) cmp.ProductEditionId)
                select new CustomerMaintenance
                {
                    Id                         = customer.Id,
                    Company                    = customer.Name,
                    FirstName                  = customer.Contact.FirstOrDefault(c => c.StatusId.HasValue && c.StatusId == 1).FirstName,
                    LastName                   = customer.Contact.FirstOrDefault(c => c.StatusId.HasValue && c.StatusId == 1).LastName,
                    Email                      = customer.Email,
                    Phone                      = string.IsNullOrWhiteSpace(customer.Phone1) ? customer.Phone2 : customer.Phone1,
                    ManualUpdate               = customer.ManualUpdate,
                    HasBtos                    = cmp.ProductEditionId == (int) ProductEditionType.Btos,
                    BtosQuantity               = cmp.ProductEditionId == (int) ProductEditionType.Btos ? cmp.Quantity : 0,
                    HasGlobal                  = cmp.ProductEditionId == (int) ProductEditionType.Global,
                    GlobalQuantity             = cmp.ProductEditionId == (int) ProductEditionType.Global ? cmp.Quantity : 0,
                    HasOffsider                = cmp.ProductEditionId == (int) ProductEditionType.Offsider,
                    OffsiderQuantity           = cmp.ProductEditionId == (int) ProductEditionType.Offsider ? cmp.Quantity : 0,
                    HasCubit                   = allCubitProductTypes.Contains((ProductEditionType) cmp.ProductEditionId),
                    HasCubitEnterpriseFixed    = cmp.ProductEditionId == (int) ProductEditionType.CubitEnterpriseFixed,
                    HasCubitEnterpriseFloating = cmp.ProductEditionId == (int) ProductEditionType.CubitEnterpriseFloating,
                    CubitQuantity              = allCubitProductTypes.Contains((ProductEditionType) cmp.ProductEditionId) ? cmp.Quantity : 0,
                    CubitEnterpriseQuantity    = cubiEnterpriseProductTypes.Contains((ProductEditionType) cmp.ProductEditionId) ? cmp.Quantity : 0,
                    HasScp                     = cmp.ProductEditionId == (int) ProductEditionType.WithScp,
                    ScpQuantity                = cmp.ProductEditionId == (int) ProductEditionType.WithScp ? cmp.Quantity : 0,
                    DistributorId              = customer.DistributorId,
                    Distributor                = customer.Distributor.Name,
                    InSoftwareAssurance        = cm.MaintenanceDate > DateTime.Now,
                    CustomerMaintenanceDate    = cm.MaintenanceDate,
                    HasNetworkLicense          = cmp.LicenseType == (int) LicenseType.Network,
                    HasTerminalLicense         = cmp.LicenseType == (int) LicenseType.Terminal,
                    HasStandaloneLicense       = cmp.LicenseType == (int) LicenseType.Standalone,
                    Country                    = customer.Address.Country.FullName,
                    State                      = customer.Address.State.FullName,
                    Status                     = customer.CustomerStatus.Name,
                    Type                       = customer.CustomerType.Name
                }).ToListAsync();

            return maintenanceCustomers;
        }


        public static async Task<List<CustomerMaintenance>> GetCustomersWhoShouldBeMarkedAsManualUpdateAsync(IEnumerable<ProductEditionType> products, Func<CustomerMaintenance, bool> expression)
        {
            var customerMaintenances = await GetCustomerMaintenancesAsync(products);
            return GroupCustomerMaintenances(customerMaintenances.Where(expression)).Where(c => c.HasStandaloneLicense && (c.HasNetworkLicense || c.HasTerminalLicense) || c.HasNetworkLicense && c.HasTerminalLicense).ToList();
        }

        public static async Task<List<CustomerMaintenance>> GetGroupedCustomerMaintenancesAsync(IEnumerable<ProductEditionType> products, Func<CustomerMaintenance, bool> expression)
        {
            var customerMaintenances = await GetCustomerMaintenancesAsync(products);
            return GroupCustomerMaintenances(customerMaintenances.Where(expression)).ToList();
        }

        public static async Task<List<CustomerMaintenance>> GetGroupedCustomerMaintenancesAsync(Func<CustomerMaintenance, bool> expression)
        {
            var products             = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType));
            var customerMaintenances = await GetCustomerMaintenancesAsync(products);
            return GroupCustomerMaintenances(customerMaintenances.Where(expression)).ToList();
        }

        private static IEnumerable<CustomerMaintenance> GroupCustomerMaintenances(IEnumerable<CustomerMaintenance> customerMaintenances)
        {
            return customerMaintenances.GroupBy(cm => cm.Id).Select(grp => new CustomerMaintenance
            {
                Id                         = grp.Key,
                Company                    = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.Company))?.Company,
                FirstName                  = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.FirstName))?.FirstName,
                LastName                   = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.LastName))?.LastName,
                Email                      = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.Email))?.Email,
                Phone                      = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.Phone))?.Phone,
                ManualUpdate               = grp.Any(cm => cm.ManualUpdate),
                HasBtos                    = grp.Any(cm => cm.HasBtos),
                BtosQuantity               = grp.Any(cm => cm.HasBtos) ? grp.Where(cm => cm.HasBtos).Sum(cm => cm.BtosQuantity) : 0,
                HasGlobal                  = grp.Any(cm => cm.HasGlobal),
                GlobalQuantity             = grp.Any(cm => cm.HasGlobal) ? grp.Where(cm => cm.HasGlobal).Sum(cm => cm.GlobalQuantity) : 0,
                HasOffsider                = grp.Any(cm => cm.HasOffsider),
                OffsiderQuantity           = grp.Any(cm => cm.HasOffsider) ? grp.Where(cm => cm.HasOffsider).Sum(cm => cm.OffsiderQuantity) : 0,
                HasCubit                   = grp.Any(cm => cm.HasCubit),
                HasCubitEnterpriseFixed    = grp.Any(cm => cm.HasCubitEnterpriseFixed),
                HasCubitEnterpriseFloating = grp.Any(cm => cm.HasCubitEnterpriseFloating),
                CubitQuantity              = grp.Any(cm => cm.HasCubit) ? grp.Where(cm => cm.HasCubit).Sum(cm => cm.CubitQuantity) : 0,
                CubitEnterpriseQuantity    = grp.Any(cm => cm.HasCubit) ? grp.Where(cm => cm.HasCubitEnterpriseFixed || cm.HasCubitEnterpriseFloating).Sum(cm => cm.CubitEnterpriseQuantity) : 0,
                HasScp                     = grp.Any(cm => cm.HasScp),
                ScpQuantity                = grp.Any(cm => cm.HasScp) ? grp.Where(cm => cm.HasScp).Sum(cm => cm.ScpQuantity) : 0,
                DistributorId              = grp.Any(cm => cm.InSoftwareAssurance) ? grp.FirstOrDefault(cm => cm.InSoftwareAssurance && cm.DistributorId.HasValue)?.DistributorId : grp.FirstOrDefault(cm => cm.DistributorId.HasValue)?.DistributorId,
                Distributor                = grp.Any(cm => cm.InSoftwareAssurance) ? grp.FirstOrDefault(cm => cm.InSoftwareAssurance && cm.DistributorId.HasValue)?.Distributor : grp.FirstOrDefault(cm => cm.DistributorId.HasValue)?.Distributor,
                InSoftwareAssurance        = grp.Any(cm => cm.InSoftwareAssurance),
                CustomerMaintenanceDate    = grp.Any(cm => cm.InSoftwareAssurance) ? grp.Where(cm => cm.InSoftwareAssurance).Max(cm => cm.CustomerMaintenanceDate ?? new DateTime()) : grp.Max(cm => cm.CustomerMaintenanceDate ?? new DateTime()),
                HasNetworkLicense          = grp.Any(cm => cm.HasNetworkLicense),
                HasTerminalLicense         = grp.Any(cm => cm.HasTerminalLicense),
                HasStandaloneLicense       = grp.Any(cm => cm.HasStandaloneLicense),
                Country                    = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.Country))?.Country,
                State                      = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.State))?.State,
                Status                     = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.Status))?.Status,
                Type                       = grp.FirstOrDefault(cm => !string.IsNullOrEmpty(cm.Type))?.Type
            });
        }
    }
}