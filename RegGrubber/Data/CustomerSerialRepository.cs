﻿using Microsoft.EntityFrameworkCore;
using RegGrubber.Enums;
using RegGrubber.Models;
using RegGrubber.ReginaldDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RegGrubber.Data
{
    public static class CustomerSerialRepository
    {
        private static readonly ReginaldContext Context;

        static CustomerSerialRepository()
        {
            Context = new ReginaldContext();
        }

        public static Task<List<SerialSummary>> GetCustomersWithSerialsAsync(Expression<Func<CustomerSerial, bool>> expression)
        {
            return GetSerialSummaries(Context.CustomerSerial.Where(expression));
        }

        public static Task<List<SerialSummary>> GetCustomersWithSerialsAsync()
        {
            return GetSerialSummaries(Context.CustomerSerial);
        }

        private static Task<List<SerialSummary>> GetSerialSummaries(IQueryable<CustomerSerial> serials)
        {
           return serials.Select(serial => new SerialSummary
                              {
                                  CustomerId         = serial.CustomerProduct.Customer.Id,
                                  Name               = serial.CustomerProduct.Customer.Name,
                                  Email              = serial.CustomerProduct.Customer.Email,
                                  Phone              = serial.CustomerProduct.Customer.Phone1,
                                  Serial             = serial.Serial,
                                  Copies             = serial.Copies ?? 0,
                                  ProductEditionType = (ProductEditionType)serial.ProductLicense.EditionId,
                                  ProductVersionType = (ProductVersionType)serial.ProductLicense.VersionId,
                                  LicenseType        = (LicenseType)serial.CustomerProduct.LicenseTypeId,
                                  SupportState       = serial.SupportState
                              }).Distinct().ToListAsync();
        }
    }
}