﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;

namespace RegGrubber.Utils
{
    internal static class CsvParser
    {
        private static readonly string Path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\RegGrubber";

        public static void Print<T>(IEnumerable<T> customerMaintenances, string fileName)
        {
            CreateDirectoryIfNotExists();

            using (var textWriter = File.CreateText($@"{Path}\{fileName}.csv"))
            using (var csv = new CsvWriter(textWriter))
            {
                csv.WriteRecords(customerMaintenances);
            }

            Console.WriteLine("Done!");
        }

        public static IEnumerable<T> Read<T>(string filePath)
        {
            try
            {
                var reader = new StreamReader(filePath);
                var csv    = new CsvReader(reader, new Configuration {BadDataFound = null});

                var records = csv.GetRecords<T>();

                return records;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static void CreateDirectoryIfNotExists()
        {
            try
            {
                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}