﻿using System;

namespace RegGrubber.Utils
{
    public static class Decoration
    {
        public static void DrawTextProgressBar(int progress, int total, string message = "Processing..")
        {
            if (progress < 0 || progress >= total)
                throw new InvalidOperationException("currElement out of range");

            var percent = (100 * (progress + 1)) / total;
            Console.Write($"\r{message} {percent}% complete");
            if (progress == total - 1)
            {
                Console.WriteLine(Environment.NewLine);
            }
        }

        public static void PrintLogo()
        {
            Console.WriteLine(@"
              _____               _____            _     _               
             |  __ \             / ____|          | |   | |              
             | |__) |___  __ _  | |  __ _ __ _   _| |__ | |__   ___ _ __ 
             |  _  // _ \/ _` | | | |_ | '__| | | | '_ \| '_ \ / _ \ '__|
             | | \ \  __/ (_| | | |__| | |  | |_| | |_) | |_) |  __/ |   
             |_|  \_\___|\__, |  \_____|_|   \__,_|_.__/|_.__/ \___|_|   
                          __/ |                                          
                         |___/   
            ");
        }
    }
}
