﻿namespace RegGrubber.Models
{
    public class QlmLicenseTable
    {
        public int Id { get; set; }
        public string ActivationKey { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public int ProductId { get; set; }
        public string MajorVersion { get; set; }
        public string MinorVersion { get; set; }
        public int ActivationCount { get; set; }
        public int NumLicenses { get; set; }
        public int AvailableLicenses { get; set; }
        public int FloatingSeats { get; set; }
        public string Features { get; set; }
        public bool Evaluation { get; set; }
        public bool Student { get; set; }
    }
}
