﻿using System;

namespace RegGrubber.Models
{
    public class CustomerMaintenance
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public bool ManualUpdate { get; set; }
        public bool InSoftwareAssurance { get; set; }
        public bool HasBtos { get; set; }
        public int BtosQuantity { get; set; }
        public bool HasGlobal { get; set; }
        public int GlobalQuantity { get; set; }
        public bool HasOffsider { get; set; }
        public int OffsiderQuantity { get; set; }
        public bool HasCubit { get; set; }
        public bool HasCubitEnterpriseFixed { get; set; }
        public bool HasCubitEnterpriseFloating { get; set; }
        public int CubitQuantity { get; set; }
        public int CubitEnterpriseQuantity { get; set; }
        public bool HasScp { get; set; }
        public int ScpQuantity { get; set; }
        public bool HasNetworkLicense { get; set; }
        public bool HasTerminalLicense { get; set; }
        public bool HasStandaloneLicense { get; set; }
        public int? DistributorId { get; set; }
        public string Distributor { get; set; }
        public DateTime? CustomerMaintenanceDate { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
    }
}