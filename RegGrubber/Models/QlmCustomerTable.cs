﻿namespace RegGrubber.Models
{
    public class QlmCustomerTable
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Email { get; set; }
    }
}
