﻿namespace RegGrubber.Models
{
    public class ProductQuantityMistmatch
    {
        public int CustomerId { get; set; }
        public int CubitSerialsQuantityDifference { get; set; }
        public int GlobalSerialsQuantityDifference { get; set; }
        public int OffsiderSerialsQuantityDifference { get; set; }
    }
}
