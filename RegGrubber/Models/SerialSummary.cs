﻿using RegGrubber.Enums;

namespace RegGrubber.Models
{
    public class SerialSummary
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Serial { get; set; }
        public int Copies { get; set; }
        public ProductEditionType ProductEditionType { get; set; }
        public ProductVersionType ProductVersionType { get; set; }
        public LicenseType LicenseType { get; set; }
        public int SupportState { get; set; }
    }
}
