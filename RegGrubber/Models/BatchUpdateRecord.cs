﻿namespace RegGrubber.Models
{
    internal class BatchUpdateRecord
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public int? CopiesPerLicense { get; set; }
        public int? Activations { get; set; }
        public string ProductType { get; set; }
        public string License { get; set; }
        public string Version { get; set; }
        public string NewSerial { get; set; }
        public string Attention { get; set; }
    }
}