﻿namespace RegGrubber.Models
{
    public class DeployLxReport
    {
        public string SerialNumber { get; set; }
        public int IsValid { get; set; }
        public string LastChecked { get; set; }
        public string LicenseXml { get; set; }
    }
}
