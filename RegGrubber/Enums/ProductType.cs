﻿namespace RegGrubber.Enums
{
    public enum ProductType
    {
        Global    = 7,
        Offsider  = 14,
        Cubit     = 27,
        Bartender = 28
    }
}