﻿namespace RegGrubber.Enums
{
    public enum LicenseType
    {
        Standalone = 1,
        Terminal = 2,
        Network = 3,
        Cloud = 7
    }
}