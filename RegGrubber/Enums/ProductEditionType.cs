﻿namespace RegGrubber.Enums
{
    public enum ProductEditionType
    {   None                      = 0,
        Btos                      = 6,
        Global                    = 9,
        Offsider                  = 13,
        Scp                       = 16,
        Student                   = 17,
        CubitProfessionalFixed    = 22,
        CubitProfessionalFloating = 23,
        CubitStandardFixed        = 24,
        CubitStandardFloating     = 25,
        CubitStudent              = 26,
        CubitEducation            = 27,
        CubitEvaluation           = 28,
        CubitLiteFixed            = 29,
        CubitLiteFloating         = 30,
        WithScp                   = 31,
        CubitEnterpriseFixed      = 32,
        CubitEnterpriseFloating   = 33

    }
}
