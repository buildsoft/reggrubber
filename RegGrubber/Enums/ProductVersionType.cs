﻿namespace RegGrubber.Enums
{
    public enum ProductVersionType
    {
        Cubit4 = 8,
        Cubit5 = 10,
        Cubit6 = 13,
        Cubit7 = 15,
        Cubit8 = 19,
        Cubit9 = 23
    }
}
