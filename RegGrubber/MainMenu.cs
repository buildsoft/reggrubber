﻿using RegGrubber.Utils;
using ShellProgressBar;
using System;
using System.Threading.Tasks;

namespace RegGrubber
{
    public static class MainMenu
    {
        public static async Task BuildMenu()
        {
            var option = "yes";

            var progressBarOptions = new ProgressBarOptions
            {
                ProgressCharacter   = '─',
                ProgressBarOnBottom = true
            };

            while (option.Equals("yes", StringComparison.OrdinalIgnoreCase) || option.Equals("y", StringComparison.OrdinalIgnoreCase))
            {
                Decoration.PrintLogo();

                Console.WriteLine(
                    "   Choose Print Option:"
                    + "\n       1. Print Cubit customers with Enterprise, Fixed license"
                    + "\n       2. Print Customers List"
                    + "\n       3. Print Cubit Customers out of Software Assurance"
                    + "\n       4. Print Customers out of Software Assurance"
                    + "\n       5. Print Network or Terminal Customers"
                    + "\n       6. Print Customers with Global under SA"
                    + "\n       7. Print Customers with SCP"
                    + "\n       8. Print Customers with Serials"
                    + "\n       9. Print Manual Update Customers"
                    + "\n       10.Print Customers who needs Manual Update flag"
                    + "\n       11.Print Customers By License Group"
                    + "\n       12.Print Customers count by License Group"
                    + "\n       13.Print Enterprise Cubit Customers"
                    + "\n       14.ReadCsvBatchReport"
                    + "\n       15.Print customer serials"
                    + "\n       16.Print Missing Customers after BT"
                    + "\n       17.Print Customers with mistmach Products or Key Quantities"
                    + "\n       18.Print Customers with Professional Fixed Network Terminal"
                    + "\n       19.Print Customers based on DeployLx Report"
                    + "\n       20.Print Customers based on Qlm Report"
                    + "\n       21.Print Seats"
                    + "\n       22.Print All Customers Summary"
                );

                Console.Write("   Index: ");
                short.TryParse(Console.ReadLine(), out var index);
                switch (index)
                {
                    case 1:
                        await MenuOptions.PrintCustomersWithCubitEnterpriseFloatingFixedAsync(progressBarOptions);
                        break;
                    case 2:
                        await MenuOptions.PrintAllCustomerMaintenancesAsync(progressBarOptions);
                        break;
                    case 3:
                        await MenuOptions.PrintCubitCustomerOutOfSoftwareAssuranceAsync(progressBarOptions);
                        break;
                    case 4:
                        await MenuOptions.PrintCustomersOutOfSoftwareAssuranceAsync(progressBarOptions);
                        break;
                    case 5:
                        await MenuOptions.PrintNetworkOrTerminalCustomersAsync(progressBarOptions);
                        break;
                    case 6:
                        await MenuOptions.PrintCustomersWithGlobalAsync(progressBarOptions);
                        break;
                    case 7:
                        await MenuOptions.PrintCustomersWithScpAsync(progressBarOptions);
                        break;
                    case 8:
                        await MenuOptions.PrintCustomersWithSerialsAsync(progressBarOptions);
                        break;
                    case 9:
                        await MenuOptions.PrintManualUpdateCustomersAsync(progressBarOptions);
                        break;
                    case 10:
                        await MenuOptions.PrintCustomerMaintenancesWhichRequreToBeFlagedAsManualUpdateAsync(progressBarOptions);
                        break;
                    case 11:
                        await MenuOptions.PrintCustomerMaintenancesByLicenseGroupsAsync(progressBarOptions);
                        break;
                    case 12:
                        await MenuOptions.PrintCountOfCustomersForEachGroupAsync(progressBarOptions);
                        break;
                    case 13:
                        await MenuOptions.PrintEnterpriseCubitUsersAsync(progressBarOptions);
                        break;
                    case 14:
                        await MenuOptions.ReadCsvBatchReport(progressBarOptions);
                        break;
                    case 15:
                        await MenuOptions.PrintCustomerSerialsAsync(progressBarOptions);
                        break;
                    case 16:
                        await MenuOptions.PrintGlobalAndCubitCustomersWithSerialsAsync(progressBarOptions);
                        break;
                    case 17:
                        await MenuOptions.PrintCustomersWithMistmachProductOrSerialCopiesAsync(progressBarOptions);
                        break;
                    case 18:
                        await MenuOptions.PrintCustomerWithProfessionalFixedNetworkTerminal(progressBarOptions);
                        break;
                    case 19:
                        await MenuOptions.ReadCsvDeploylxReportAsync(progressBarOptions);
                        break;
                    case 20:
                        await MenuOptions.ReadCsvQlmReportAsync(progressBarOptions);
                        break;
                    case 21:
                        await MenuOptions.PrintLicenseInfoAsync(progressBarOptions);
                        break;
                    case 22:
                        await MenuOptions.PrintAllCustomersSummaryAsync(progressBarOptions);
                        break;
                    default:
                        Console.WriteLine("No index found.");
                        break;
                }

                Console.Write("Continue (yes/no): ");
                option = Console.ReadLine();
                Console.Clear();
            }
        }
    }
}
