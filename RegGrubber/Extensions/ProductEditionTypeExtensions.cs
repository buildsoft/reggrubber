﻿using RegGrubber.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RegGrubber.Extensions
{
    public static class ProductEditionTypeExtensions
    {
        public static IEnumerable<ProductEditionType> GetAllProducts(Type productEnumType)
        {
            return !productEnumType.IsEnum ? null : Enum.GetValues(productEnumType).Cast<ProductEditionType>();
        }

        public static IEnumerable<ProductEditionType> GetAllCubitProductTypes(Type productEnumType)
        {
            if (!productEnumType.IsEnum)
            {
                yield return ProductEditionType.None;
            }

            foreach (ProductEditionType product in Enum.GetValues(productEnumType))
            {
                if (product >= (ProductEditionType) 22 && product != (ProductEditionType) 31 || product == (ProductEditionType)32 || product == (ProductEditionType)33)
                    yield return product;
            }
        }

        public static IEnumerable<ProductEditionType> GetWithoutEnterpriseCubitProductTypes(Type productEnumType)
        {
            if (!productEnumType.IsEnum)
            {
                yield return ProductEditionType.None;
            }

            foreach (ProductEditionType product in Enum.GetValues(productEnumType))
            {
                if (product >= (ProductEditionType)22 && product != (ProductEditionType)31)
                    yield return product;
            }
        }

        public static IEnumerable<ProductEditionType> GetEnterpriseCubitProductTypes(Type productEnumType)
        {
            if (!productEnumType.IsEnum)
            {
                yield return ProductEditionType.None;
            }

            foreach (ProductEditionType product in Enum.GetValues(productEnumType))
            {
                if (product == (ProductEditionType)32 || product == (ProductEditionType)33)
                    yield return product;
            }
        }

        public static IEnumerable<ProductEditionType> GetCubitWithScpProductTypes(Type productEnumType)
        {
            if (!productEnumType.IsEnum)
            {
                yield return ProductEditionType.None;
            }

            var products = new List<ProductEditionType>();
            foreach (ProductEditionType product in Enum.GetValues(productEnumType))
            {
                if (product >= (ProductEditionType)22 && product != (ProductEditionType)31)
                    products.Add(product);
            }

            products.Add(ProductEditionType.Scp);
            products.Add(ProductEditionType.WithScp);

            foreach (var productType in products)
            {
                yield return productType;
            }
        }

        public static IEnumerable<ProductEditionType> GetManualUpdateProductsGroup(Type productEnumType)
        {
            if (!productEnumType.IsEnum)
            {
                yield return ProductEditionType.None;
            }

            var products = new List<ProductEditionType>();

            foreach (ProductEditionType product in Enum.GetValues(productEnumType))
            {
                if (product >= (ProductEditionType) 22 && product != (ProductEditionType) 31)
                    products.Add(product);
            }

            products.Add(ProductEditionType.WithScp);
            products.Add(ProductEditionType.Global);
            products.Add(ProductEditionType.Offsider);

            foreach (var productType in products)
            {
                yield return productType;
            }
        }
    }
}