﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Distributor
    {
        public Distributor()
        {
            Contact = new HashSet<Contact>();
            Customer = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? AddressId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual Address Address { get; set; }
        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
    }
}
