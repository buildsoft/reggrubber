﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class RolePermissions
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string CustomerForm { get; set; }
        public string RegisterUser { get; set; }
        public string ChangePassword { get; set; }

        public virtual Role RoleNameNavigation { get; set; }
    }
}
