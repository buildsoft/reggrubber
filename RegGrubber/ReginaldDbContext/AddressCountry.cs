﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class AddressCountry
    {
        public AddressCountry()
        {
            Address = new HashSet<Address>();
            AddressState = new HashSet<AddressState>();
        }

        public short Id { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string SupportLowerBound { get; set; }
        public string SupportUpperBound { get; set; }

        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<AddressState> AddressState { get; set; }
    }
}
