﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Role
    {
        public Role()
        {
            RolePermissions = new HashSet<RolePermissions>();
            User = new HashSet<User>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual ICollection<RolePermissions> RolePermissions { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
