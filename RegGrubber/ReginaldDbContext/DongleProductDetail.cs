﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class DongleProductDetail
    {
        public string DongleNumber { get; set; }
        public int ProductId { get; set; }

        public virtual Dongle DongleNumberNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
