﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Customer
    {
        public Customer()
        {
            Contact = new HashSet<Contact>();
            Contracts = new HashSet<Contracts>();
            CustomerMaintenance = new HashSet<CustomerMaintenance>();
            CustomerMaintenanceQuote = new HashSet<CustomerMaintenanceQuote>();
            CustomerNote = new HashSet<CustomerNote>();
            CustomerProduct = new HashSet<CustomerProduct>();
            CustomerRequiresAttention = new HashSet<CustomerRequiresAttention>();
            HistoryCustomerName = new HashSet<HistoryCustomerName>();
            Quote = new HashSet<Quote>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? DistributorId { get; set; }
        public int? AddressId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? CustomerStatusId { get; set; }
        public string UserName { get; set; }
        public string Fax { get; set; }
        public string Number { get; set; }
        public string SupportNumber { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? MaintenanceDate { get; set; }
        public decimal? MaintenanceAmount { get; set; }
        public DateTime? DateModified { get; set; }
        public bool ManualUpdate { get; set; }
        public string AccountsEmail { get; set; }
        public int? ParentId { get; set; }
        public int? InternationalAgreement { get; set; }

        public virtual Address Address { get; set; }
        public virtual CustomerStatus CustomerStatus { get; set; }
        public virtual CustomerType CustomerType { get; set; }
        public virtual Distributor Distributor { get; set; }
        public virtual User UserNameNavigation { get; set; }
        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Contracts> Contracts { get; set; }
        public virtual ICollection<CustomerMaintenance> CustomerMaintenance { get; set; }
        public virtual ICollection<CustomerMaintenanceQuote> CustomerMaintenanceQuote { get; set; }
        public virtual ICollection<CustomerNote> CustomerNote { get; set; }
        public virtual ICollection<CustomerProduct> CustomerProduct { get; set; }
        public virtual ICollection<CustomerRequiresAttention> CustomerRequiresAttention { get; set; }
        public virtual ICollection<HistoryCustomerName> HistoryCustomerName { get; set; }
        public virtual ICollection<Quote> Quote { get; set; }
    }
}
