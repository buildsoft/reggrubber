﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerSerialNote
    {
        public int Id { get; set; }
        public int CustomerSerialId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }

        public virtual CustomerSerial CustomerSerial { get; set; }
    }
}
