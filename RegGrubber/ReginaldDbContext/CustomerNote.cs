﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerNote
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public DateTime? DateCreated { get; set; }
        public string UserName { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
