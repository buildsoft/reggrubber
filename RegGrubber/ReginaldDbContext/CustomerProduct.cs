﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerProduct
    {
        public CustomerProduct()
        {
            ContractCustomerProductMapping = new HashSet<ContractCustomerProductMapping>();
            CustomerSerial = new HashSet<CustomerSerial>();
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int Copies { get; set; }
        public DateTime? DateModified { get; set; }
        public string Note { get; set; }
        public int EditionId { get; set; }
        public int LicenseTypeId { get; set; }
        public byte? ContractType { get; set; }
        public bool? IsActive { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ProductCatelog Edition { get; set; }
        public virtual ProductLicenseType LicenseType { get; set; }
        public virtual ICollection<ContractCustomerProductMapping> ContractCustomerProductMapping { get; set; }
        public virtual ICollection<CustomerSerial> CustomerSerial { get; set; }
    }
}
