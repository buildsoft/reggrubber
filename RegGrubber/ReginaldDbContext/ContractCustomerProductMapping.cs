﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ContractCustomerProductMapping
    {
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public int? CustomerProductId { get; set; }

        public virtual Contracts Contract { get; set; }
        public virtual CustomerProduct CustomerProduct { get; set; }
    }
}
