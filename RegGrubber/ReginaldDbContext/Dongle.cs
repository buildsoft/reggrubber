﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Dongle
    {
        public Dongle()
        {
            DongleNote = new HashSet<DongleNote>();
            DongleProductDetail = new HashSet<DongleProductDetail>();
        }

        public string DongleNumber { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? CustomerNameCrc { get; set; }
        public string SupportNumber { get; set; }
        public string MaxVersion { get; set; }
        public int? MaxVersionCrc { get; set; }
        public int? MaxNumberOfStarts { get; set; }
        public int? MaxNumberOfStartsCrc { get; set; }
        public int? DateBurnt { get; set; }
        public int DateExpired { get; set; }
        public int? DateExpiredCrc { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string UpdateStatus { get; set; }
        public int? DongleTypeId { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateCreated { get; set; }
        public string UserCreated { get; set; }
        public string UserModified { get; set; }
        public string MaxVersion2 { get; set; }
        public string MaxVersion3 { get; set; }
        public string ErrString { get; set; }
        public bool? QueueForUpdate { get; set; }
        public int DateExpired2 { get; set; }
        public int DateExpired3 { get; set; }
        public bool? IsValid { get; set; }
        public DateTime UpdateStatusChanged { get; set; }

        public virtual ICollection<DongleNote> DongleNote { get; set; }
        public virtual ICollection<DongleProductDetail> DongleProductDetail { get; set; }
    }
}
