﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerSerialActivation
    {
        public int Id { get; set; }
        public int CustomerSerialId { get; set; }
        public string CreatedBy { get; set; }
        public string ActivationCode { get; set; }
        public string MachineCode { get; set; }
        public int ActivationDays { get; set; }
        public DateTime DateActivated { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual CustomerSerial CustomerSerial { get; set; }
    }
}
