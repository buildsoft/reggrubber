﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class QuoteDetail
    {
        public int Id { get; set; }
        public int? QuoteId { get; set; }
        public decimal? Price { get; set; }
        public short? NumberUsers { get; set; }
        public short? Copies { get; set; }
        public string Note { get; set; }
        public int ProductCatelogId { get; set; }

        public virtual ProductCatelog ProductCatelog { get; set; }
        public virtual Quote Quote { get; set; }
    }
}
