﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Quote
    {
        public Quote()
        {
            QuoteDetail = new HashSet<QuoteDetail>();
            QuoteNote = new HashSet<QuoteNote>();
        }

        public int Id { get; set; }
        public int? ContactId { get; set; }
        public int? CustomerId { get; set; }
        public int? QuoteTypeId { get; set; }
        public string UserName { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? ValidUntil { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Total { get; set; }
        public string DiscountUserName { get; set; }
        public byte? DiscountType { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual QuoteType QuoteType { get; set; }
        public virtual ICollection<QuoteDetail> QuoteDetail { get; set; }
        public virtual ICollection<QuoteNote> QuoteNote { get; set; }
    }
}
