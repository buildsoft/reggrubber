﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerMaintenanceQuoteLineItem
    {
        public int Id { get; set; }
        public int? QuoteId { get; set; }
        public int? ProductId { get; set; }
        public decimal? Price { get; set; }
        public int? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public string Total { get; set; }
    }
}
