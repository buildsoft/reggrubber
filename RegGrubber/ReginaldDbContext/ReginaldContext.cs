﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ReginaldContext : DbContext
    {
        public ReginaldContext()
        {
        }

        public ReginaldContext(DbContextOptions<ReginaldContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<AddressCountry> AddressCountry { get; set; }
        public virtual DbSet<AddressState> AddressState { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<ContactNote> ContactNote { get; set; }
        public virtual DbSet<ContractCustomerProductMapping> ContractCustomerProductMapping { get; set; }
        public virtual DbSet<Contracts> Contracts { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerHistoryMapping> CustomerHistoryMapping { get; set; }
        public virtual DbSet<CustomerMaintenance> CustomerMaintenance { get; set; }
        public virtual DbSet<CustomerMaintenanceProduct> CustomerMaintenanceProduct { get; set; }
        public virtual DbSet<CustomerMaintenanceQuote> CustomerMaintenanceQuote { get; set; }
        public virtual DbSet<CustomerMaintenanceQuoteLineItem> CustomerMaintenanceQuoteLineItem { get; set; }
        public virtual DbSet<CustomerNote> CustomerNote { get; set; }
        public virtual DbSet<CustomerProduct> CustomerProduct { get; set; }
        public virtual DbSet<CustomerRequiresAttention> CustomerRequiresAttention { get; set; }
        public virtual DbSet<CustomerSerial> CustomerSerial { get; set; }
        public virtual DbSet<CustomerSerialActivation> CustomerSerialActivation { get; set; }
        public virtual DbSet<CustomerSerialNote> CustomerSerialNote { get; set; }
        public virtual DbSet<CustomerStatus> CustomerStatus { get; set; }
        public virtual DbSet<CustomerType> CustomerType { get; set; }
        public virtual DbSet<CustomerUpdateHistory> CustomerUpdateHistory { get; set; }
        public virtual DbSet<Distributor> Distributor { get; set; }
        public virtual DbSet<Dongle> Dongle { get; set; }
        public virtual DbSet<DongleNote> DongleNote { get; set; }
        public virtual DbSet<DongleProductDetail> DongleProductDetail { get; set; }
        public virtual DbSet<DongleType> DongleType { get; set; }
        public virtual DbSet<DongleUpload> DongleUpload { get; set; }
        public virtual DbSet<HistoryCustomerName> HistoryCustomerName { get; set; }
        public virtual DbSet<LicenseMethod> LicenseMethod { get; set; }
        public virtual DbSet<Logs> Logs { get; set; }
        public virtual DbSet<MaintenanceHistory> MaintenanceHistory { get; set; }
        public virtual DbSet<MaintenanceNote> MaintenanceNote { get; set; }
        public virtual DbSet<MaintenanceNoteHistory> MaintenanceNoteHistory { get; set; }
        public virtual DbSet<MaintenanceProductHistory> MaintenanceProductHistory { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductCatelog> ProductCatelog { get; set; }
        public virtual DbSet<ProductLicense> ProductLicense { get; set; }
        public virtual DbSet<ProductLicenseType> ProductLicenseType { get; set; }
        public virtual DbSet<ProductVersion> ProductVersion { get; set; }
        public virtual DbSet<Quote> Quote { get; set; }
        public virtual DbSet<QuoteDetail> QuoteDetail { get; set; }
        public virtual DbSet<QuoteNote> QuoteNote { get; set; }
        public virtual DbSet<QuoteType> QuoteType { get; set; }
        public virtual DbSet<ReportNotes> ReportNotes { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RolePermissions> RolePermissions { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserPermissions> UserPermissions { get; set; }

        // Unable to generate entity type for table 'dbo.ProductAvailableLicense'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CustomerSerialHistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CustomerProductHistory'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CustomerContractHistory'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=Reginald;Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PostCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.Street)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Suburb)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_Address_AddressCountry");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_Address_AddressState");
            });

            modelBuilder.Entity<AddressCountry>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShortName)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SupportLowerBound)
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0000000')");

                entity.Property(e => e.SupportUpperBound)
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0000000')");
            });

            modelBuilder.Entity<AddressState>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShortName)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SupportLowerBound)
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0000000')");

                entity.Property(e => e.SupportUpperBound)
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0000000')");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.AddressState)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_AddressState_AddressCountry");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => new { e.CustomerId, e.DistributorId, e.Name })
                    .HasName("IX_Contact")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DistributorId).HasColumnName("DistributorID");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Prefix)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Contact)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Contact_Customer");

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.Contact)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Contact_Distributor");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Contact)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Contact_CustomerStatus");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Contact)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_Contact_CustomerType");
            });

            modelBuilder.Entity<ContactNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK_Notes")
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => e.ContactId)
                    .HasName("IX_ContactNote")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.ContactNote)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_ContactNote_Contact");
            });

            modelBuilder.Entity<ContractCustomerProductMapping>(entity =>
            {
                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.ContractCustomerProductMapping)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK_CustomerProductContractMapping_CustomerProduct");

                entity.HasOne(d => d.CustomerProduct)
                    .WithMany(p => p.ContractCustomerProductMapping)
                    .HasForeignKey(d => d.CustomerProductId)
                    .HasConstraintName("FK_CustomerProductContractMapping_CustomerProduct1");
            });

            modelBuilder.Entity<Contracts>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryDate).HasColumnType("date");

                entity.Property(e => e.MaintenanceId).HasColumnName("MaintenanceID");

                entity.Property(e => e.Note)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contracts_Customer");

                entity.HasOne(d => d.Maintenance)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.MaintenanceId)
                    .HasConstraintName("FK__Contracts__Maint__7345FA8E");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AccountsEmail)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.CustomerStatusId).HasColumnName("CustomerStatusID");

                entity.Property(e => e.CustomerTypeId).HasColumnName("CustomerTypeID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DistributorId).HasColumnName("DistributorID");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InternationalAgreement).HasDefaultValueSql("((0))");

                entity.Property(e => e.MaintenanceAmount).HasColumnType("money");

                entity.Property(e => e.MaintenanceDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Number)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.Phone1)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SupportNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WebAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK_Customer_Address");

                entity.HasOne(d => d.CustomerStatus)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.CustomerStatusId)
                    .HasConstraintName("FK_Customer_CustomerStatus");

                entity.HasOne(d => d.CustomerType)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.CustomerTypeId)
                    .HasConstraintName("FK_Customer_CustomerType");

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.DistributorId)
                    .HasConstraintName("FK_Customer_Distributor");

                entity.HasOne(d => d.UserNameNavigation)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.UserName)
                    .HasConstraintName("FK_Customer_User");
            });

            modelBuilder.Entity<CustomerHistoryMapping>(entity =>
            {
                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerUpdateHistoryId).HasColumnName("CustomerUpdateHistoryID");
            });

            modelBuilder.Entity<CustomerMaintenance>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaintenanceAmount).HasColumnType("money");

                entity.Property(e => e.MaintenanceDate).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerMaintenance)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Maintenance_Customer");
            });

            modelBuilder.Entity<CustomerMaintenanceProduct>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LicenseType).HasDefaultValueSql("((0))");

                entity.Property(e => e.MaintenanceId).HasColumnName("MaintenanceID");

                entity.Property(e => e.ProductEditionId).HasColumnName("ProductEditionID");

                entity.HasOne(d => d.Maintenance)
                    .WithMany(p => p.CustomerMaintenanceProduct)
                    .HasForeignKey(d => d.MaintenanceId)
                    .HasConstraintName("FK_MaintenanceProduct_Maintenance");

                entity.HasOne(d => d.ProductEdition)
                    .WithMany(p => p.CustomerMaintenanceProduct)
                    .HasForeignKey(d => d.ProductEditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MaintenanceProduct_Product");
            });

            modelBuilder.Entity<CustomerMaintenanceQuote>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateIssued).HasColumnType("date");

                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.MaintenanceId).HasColumnName("MaintenanceID");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.CustomerMaintenanceQuote)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK__CustomerM__Contr__743A1EC7");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerMaintenanceQuote)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_CustomerMaintenanceQuote_Customer");

                entity.HasOne(d => d.Maintenance)
                    .WithMany(p => p.CustomerMaintenanceQuote)
                    .HasForeignKey(d => d.MaintenanceId)
                    .HasConstraintName("FK_CustomerMaintenanceQuote_CustomerMaintenance");
            });

            modelBuilder.Entity<CustomerMaintenanceQuoteLineItem>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.QuoteId).HasColumnName("QuoteID");

                entity.Property(e => e.Total).HasMaxLength(10);
            });

            modelBuilder.Entity<CustomerNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK_CustomerNotes")
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_CustomerNote")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerNote)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CustomerNote_Customer");
            });

            modelBuilder.Entity<CustomerProduct>(entity =>
            {
                entity.Property(e => e.DateModified).HasColumnType("date");

                entity.Property(e => e.EditionId).HasColumnName("EditionID");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.LicenseTypeId).HasColumnName("LicenseTypeID");

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CustomerP__Custo__75985754");

                entity.HasOne(d => d.Edition)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.EditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerProduct_ProductCatelog");

                entity.HasOne(d => d.LicenseType)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.LicenseTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerProduct_ProductLicenseType");
            });

            modelBuilder.Entity<CustomerRequiresAttention>(entity =>
            {
                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateFixed).HasColumnType("datetime");

                entity.Property(e => e.FixedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerRequiresAttention)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerRequiresAttention_Customer");
            });

            modelBuilder.Entity<CustomerSerial>(entity =>
            {
                entity.HasIndex(e => e.Serial)
                    .HasName("UQ_Serial")
                    .IsUnique();

                entity.Property(e => e.ActivationCode)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateActivated).HasColumnType("datetime");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateEmailSent).HasColumnType("datetime");

                entity.Property(e => e.ExtensionCode)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ExtensionCount).HasDefaultValueSql("((0))");

                entity.Property(e => e.MachineCode)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SentToEmail)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SentToName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serial)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SerialLock).HasDefaultValueSql("((0))");

                entity.Property(e => e.SupportState).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CustomerProduct)
                    .WithMany(p => p.CustomerSerial)
                    .HasForeignKey(d => d.CustomerProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSerial_CustomerProduct");

                entity.HasOne(d => d.ProductLicense)
                    .WithMany(p => p.CustomerSerial)
                    .HasForeignKey(d => d.ProductLicenseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSerial_ProductLicense");
            });

            modelBuilder.Entity<CustomerSerialActivation>(entity =>
            {
                entity.Property(e => e.ActivationCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateActivated).HasColumnType("datetime");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MachineCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.HasOne(d => d.CustomerSerial)
                    .WithMany(p => p.CustomerSerialActivation)
                    .HasForeignKey(d => d.CustomerSerialId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSerialActivation_CustomerSerial");
            });

            modelBuilder.Entity<CustomerSerialNote>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.CustomerSerial)
                    .WithMany(p => p.CustomerSerialNote)
                    .HasForeignKey(d => d.CustomerSerialId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerSerialNote_CustomerSerial");
            });

            modelBuilder.Entity<CustomerStatus>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerUpdateHistory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ModifyDate).HasColumnType("date");

                entity.Property(e => e.Reason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Requester)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Distributor>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WebAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Distributor)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK_Distributor_Address");
            });

            modelBuilder.Entity<Dongle>(entity =>
            {
                entity.HasKey(e => e.DongleNumber)
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_Dongle")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.DongleNumber)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Country)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerNameCrc).HasColumnName("CustomerNameCRC");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateExpired).HasDefaultValueSql("((-73284))");

                entity.Property(e => e.DateExpired2).HasDefaultValueSql("((-73284))");

                entity.Property(e => e.DateExpired3).HasDefaultValueSql("((-73284))");

                entity.Property(e => e.DateExpiredCrc).HasColumnName("DateExpiredCRC");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DongleTypeId).HasColumnName("DongleTypeID");

                entity.Property(e => e.ErrString)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.IsValid)
                    .IsRequired()
                    .HasDefaultValueSql("(1)");

                entity.Property(e => e.MaxNumberOfStartsCrc).HasColumnName("MaxNumberOfStartsCRC");

                entity.Property(e => e.MaxVersion)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MaxVersion2)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MaxVersion3)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.MaxVersionCrc).HasColumnName("MaxVersionCRC");

                entity.Property(e => e.PostCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.QueueForUpdate)
                    .IsRequired()
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.State)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Street)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Suburb)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupportNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateStatus)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateStatusChanged)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserCreated)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserModified)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DongleNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => e.DongleNumber)
                    .HasName("IX_DongleNote")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DongleNumber)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DongleNumberNavigation)
                    .WithMany(p => p.DongleNote)
                    .HasForeignKey(d => d.DongleNumber)
                    .HasConstraintName("FK_DongleNote_Dongle");

                entity.HasOne(d => d.UserNameNavigation)
                    .WithMany(p => p.DongleNote)
                    .HasForeignKey(d => d.UserName)
                    .HasConstraintName("FK_DongleNote_User");
            });

            modelBuilder.Entity<DongleProductDetail>(entity =>
            {
                entity.HasKey(e => new { e.DongleNumber, e.ProductId });

                entity.Property(e => e.DongleNumber)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.HasOne(d => d.DongleNumberNavigation)
                    .WithMany(p => p.DongleProductDetail)
                    .HasForeignKey(d => d.DongleNumber)
                    .HasConstraintName("FK_DongleProductDetail_Dongle");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.DongleProductDetail)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DongleProductDetail_Product");
            });

            modelBuilder.Entity<DongleType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DongleUpload>(entity =>
            {
                entity.HasKey(e => e.DongleNumber);

                entity.Property(e => e.DongleNumber)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateExpired).HasColumnType("datetime");

                entity.Property(e => e.MaxVersion)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MaxVersion2)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MaxVersion3)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HistoryCustomerName>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HistoryCustomerName)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HistoryCustomerName_User");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.HistoryCustomerName)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HistoryCustomerName_Customer1");
            });

            modelBuilder.Entity<LicenseMethod>(entity =>
            {
                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DefaultTemplate)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.LicenseMethod)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LicenseMethod_Product");

                entity.HasOne(d => d.ProductLicenseType)
                    .WithMany(p => p.LicenseMethod)
                    .HasForeignKey(d => d.ProductLicenseTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LicenseMethod_ProductLicenseType");

                entity.HasOne(d => d.ProductVersion)
                    .WithMany(p => p.LicenseMethod)
                    .HasForeignKey(d => d.ProductVersionId)
                    .HasConstraintName("FK_LicenseMethod_ProductVersion");
            });

            modelBuilder.Entity<Logs>(entity =>
            {
                entity.Property(e => e.Callsite).HasMaxLength(300);

                entity.Property(e => e.Level)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Logged).HasColumnType("datetime");

                entity.Property(e => e.Logger).HasMaxLength(300);

                entity.Property(e => e.Message).IsRequired();

                entity.Property(e => e.User).HasMaxLength(300);
            });

            modelBuilder.Entity<MaintenanceHistory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('UnKnown')");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaintenanceAmount).HasColumnType("money");

                entity.Property(e => e.MaintenanceDate).HasColumnType("datetime");

                entity.Property(e => e.MaintenanceId).HasColumnName("MaintenanceID");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.MaintenanceHistory)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK__Maintenan__Contr__752E4300");

                entity.HasOne(d => d.Maintenance)
                    .WithMany(p => p.MaintenanceHistory)
                    .HasForeignKey(d => d.MaintenanceId)
                    .HasConstraintName("FK_MaintenanceHistory_Maintenance");
            });

            modelBuilder.Entity<MaintenanceNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK_MaintenanceNotes")
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.MaintenanceId).HasColumnName("MaintenanceID");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Maintenance)
                    .WithMany(p => p.MaintenanceNote)
                    .HasForeignKey(d => d.MaintenanceId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_MaintenanceNote_Maintenance");
            });

            modelBuilder.Entity<MaintenanceNoteHistory>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PK_MaintenanceHisNotes")
                    .ForSqlServerIsClustered(false);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.MaintenanceHistoryId).HasColumnName("MaintenanceHistoryID");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.MaintenanceHistory)
                    .WithMany(p => p.MaintenanceNoteHistory)
                    .HasForeignKey(d => d.MaintenanceHistoryId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_MaintenanceNoteHistory_MaintenanceHistory");
            });

            modelBuilder.Entity<MaintenanceProductHistory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.LicenseType).HasDefaultValueSql("((0))");

                entity.Property(e => e.MaintenanceHistoryId).HasColumnName("MaintenanceHistoryID");

                entity.Property(e => e.ProductEditionId).HasColumnName("ProductEditionID");

                entity.HasOne(d => d.MaintenanceHistory)
                    .WithMany(p => p.MaintenanceProductHistory)
                    .HasForeignKey(d => d.MaintenanceHistoryId)
                    .HasConstraintName("FK_MaintenanceProductHistory_MaintenanceHistory");

                entity.HasOne(d => d.ProductEdition)
                    .WithMany(p => p.MaintenanceProductHistory)
                    .HasForeignKey(d => d.ProductEditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MaintenanceProductHistory_Product");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CurrentVersionName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RequiresDongle).HasDefaultValueSql("(0)");
            });

            modelBuilder.Entity<ProductCatelog>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.IsForSale).HasDefaultValueSql("(1)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductCatelog)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductCatelog_Product");
            });

            modelBuilder.Entity<ProductLicense>(entity =>
            {
                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DaysToExpire).HasDefaultValueSql("((180))");

                entity.Property(e => e.EditionId).HasColumnName("EditionID");

                entity.Property(e => e.LicenseName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LicenseTypeId).HasColumnName("LicenseTypeID");

                entity.Property(e => e.Prefix)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.VersionId).HasColumnName("VersionID");

                entity.HasOne(d => d.Edition)
                    .WithMany(p => p.ProductLicense)
                    .HasForeignKey(d => d.EditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductLicense_ProductCatelog");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.ProductLicense)
                    .HasForeignKey(d => d.VersionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductLicense_ProductVersion");
            });

            modelBuilder.Entity<ProductLicenseType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductVersion>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BuildDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ReleaseDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.VersionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductVersion)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductVersion_Product");
            });

            modelBuilder.Entity<Quote>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnType("money");

                entity.Property(e => e.DiscountType).HasDefaultValueSql("(1)");

                entity.Property(e => e.DiscountUserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.QuoteTypeId).HasColumnName("QuoteTypeID");

                entity.Property(e => e.Total).HasColumnType("money");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValidUntil).HasColumnType("datetime");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Quote)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_Quote_Contact");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Quote)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Quote_Customer");

                entity.HasOne(d => d.QuoteType)
                    .WithMany(p => p.Quote)
                    .HasForeignKey(d => d.QuoteTypeId)
                    .HasConstraintName("FK_Quote_QuoteType");
            });

            modelBuilder.Entity<QuoteDetail>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.NumberUsers).HasDefaultValueSql("(1)");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.ProductCatelogId).HasColumnName("ProductCatelogID");

                entity.Property(e => e.QuoteId).HasColumnName("QuoteID");

                entity.HasOne(d => d.ProductCatelog)
                    .WithMany(p => p.QuoteDetail)
                    .HasForeignKey(d => d.ProductCatelogId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuoteDetail_ProductCatelog");

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteDetail)
                    .HasForeignKey(d => d.QuoteId)
                    .HasConstraintName("FK_QuoteDetail_Quote");
            });

            modelBuilder.Entity<QuoteNote>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.QuoteId).HasColumnName("QuoteID");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Quote)
                    .WithMany(p => p.QuoteNote)
                    .HasForeignKey(d => d.QuoteId)
                    .HasConstraintName("FK_QuoteNote_Quote");
            });

            modelBuilder.Entity<QuoteType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportNotes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('QUOTE')");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Name);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RolePermissions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChangePassword)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerForm)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RegisterUser)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.RoleNameNavigation)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.RoleName)
                    .HasConstraintName("FK_RolePermissions_Role");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.UserName);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(datetimefromparts((1991),(1),(1),(0),(0),(0),(0)))");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Service')");

                entity.Property(e => e.UpdatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(datetimefromparts((1991),(1),(1),(0),(0),(0),(0)))");

                entity.HasOne(d => d.RoleNavigation)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.Role)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Role");
            });

            modelBuilder.Entity<UserPermissions>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => e.UserName)
                    .HasName("IX_UserPermissions")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChangePassword)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerForm)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RegisterUser)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.UserNameNavigation)
                    .WithOne(p => p.UserPermissions)
                    .HasForeignKey<UserPermissions>(d => d.UserName)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_UserPermissions_User");
            });
        }
    }
}
