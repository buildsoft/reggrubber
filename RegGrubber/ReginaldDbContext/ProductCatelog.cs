﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ProductCatelog
    {
        public ProductCatelog()
        {
            CustomerMaintenanceProduct = new HashSet<CustomerMaintenanceProduct>();
            CustomerProduct = new HashSet<CustomerProduct>();
            MaintenanceProductHistory = new HashSet<MaintenanceProductHistory>();
            ProductLicense = new HashSet<ProductLicense>();
            QuoteDetail = new HashSet<QuoteDetail>();
        }

        public string Name { get; set; }
        public decimal? Price { get; set; }
        public string Description { get; set; }
        public bool? IsForSale { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateCreated { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? ProductType { get; set; }
        public int ContractTypes { get; set; }

        public virtual Product Product { get; set; }
        public virtual ICollection<CustomerMaintenanceProduct> CustomerMaintenanceProduct { get; set; }
        public virtual ICollection<CustomerProduct> CustomerProduct { get; set; }
        public virtual ICollection<MaintenanceProductHistory> MaintenanceProductHistory { get; set; }
        public virtual ICollection<ProductLicense> ProductLicense { get; set; }
        public virtual ICollection<QuoteDetail> QuoteDetail { get; set; }
    }
}
