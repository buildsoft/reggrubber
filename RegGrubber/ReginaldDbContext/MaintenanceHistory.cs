﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class MaintenanceHistory
    {
        public MaintenanceHistory()
        {
            MaintenanceNoteHistory = new HashSet<MaintenanceNoteHistory>();
            MaintenanceProductHistory = new HashSet<MaintenanceProductHistory>();
        }

        public int Id { get; set; }
        public int MaintenanceId { get; set; }
        public DateTime MaintenanceDate { get; set; }
        public decimal MaintenanceAmount { get; set; }
        public string Description { get; set; }
        public int? IsGrandfathered { get; set; }
        public int IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public int? ContractId { get; set; }

        public virtual Contracts Contract { get; set; }
        public virtual CustomerMaintenance Maintenance { get; set; }
        public virtual ICollection<MaintenanceNoteHistory> MaintenanceNoteHistory { get; set; }
        public virtual ICollection<MaintenanceProductHistory> MaintenanceProductHistory { get; set; }
    }
}
