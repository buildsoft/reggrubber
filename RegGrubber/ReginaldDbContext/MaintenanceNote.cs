﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class MaintenanceNote
    {
        public int Id { get; set; }
        public int? MaintenanceId { get; set; }
        public string UserName { get; set; }
        public string Note { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Name { get; set; }

        public virtual CustomerMaintenance Maintenance { get; set; }
    }
}
