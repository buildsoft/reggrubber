﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Address
    {
        public Address()
        {
            Customer = new HashSet<Customer>();
            Distributor = new HashSet<Distributor>();
        }

        public int Id { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
        public short? StateId { get; set; }
        public short? CountryId { get; set; }
        public string PostCode { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual AddressCountry Country { get; set; }
        public virtual AddressState State { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<Distributor> Distributor { get; set; }
    }
}
