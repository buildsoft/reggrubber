﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerSerial
    {
        public CustomerSerial()
        {
            CustomerSerialActivation = new HashSet<CustomerSerialActivation>();
            CustomerSerialNote = new HashSet<CustomerSerialNote>();
        }

        public int Id { get; set; }
        public int ProductLicenseId { get; set; }
        public string Serial { get; set; }
        public string MachineCode { get; set; }
        public string ActivationCode { get; set; }
        public DateTime? DateActivated { get; set; }
        public DateTime DateCreated { get; set; }
        public int? SerialLock { get; set; }
        public string CreatedBy { get; set; }
        public string SentToEmail { get; set; }
        public string SentToName { get; set; }
        public int CustomerProductId { get; set; }
        public int? Copies { get; set; }
        public string ExtensionCode { get; set; }
        public int? ExtensionCount { get; set; }
        public int LicenseMethod { get; set; }
        public int? Activations { get; set; }
        public DateTime? DateEmailSent { get; set; }
        public int SupportState { get; set; }

        public virtual CustomerProduct CustomerProduct { get; set; }
        public virtual ProductLicense ProductLicense { get; set; }
        public virtual ICollection<CustomerSerialActivation> CustomerSerialActivation { get; set; }
        public virtual ICollection<CustomerSerialNote> CustomerSerialNote { get; set; }
    }
}
