﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class DongleNote
    {
        public int Id { get; set; }
        public string DongleNumber { get; set; }
        public string Note { get; set; }
        public DateTime? DateCreated { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual Dongle DongleNumberNavigation { get; set; }
        public virtual User UserNameNavigation { get; set; }
    }
}
