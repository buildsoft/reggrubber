﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Product
    {
        public Product()
        {
            DongleProductDetail = new HashSet<DongleProductDetail>();
            LicenseMethod = new HashSet<LicenseMethod>();
            ProductCatelog = new HashSet<ProductCatelog>();
            ProductVersion = new HashSet<ProductVersion>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string CurrentVersionName { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public bool? RequiresDongle { get; set; }
        public int Id { get; set; }

        public virtual ICollection<DongleProductDetail> DongleProductDetail { get; set; }
        public virtual ICollection<LicenseMethod> LicenseMethod { get; set; }
        public virtual ICollection<ProductCatelog> ProductCatelog { get; set; }
        public virtual ICollection<ProductVersion> ProductVersion { get; set; }
    }
}
