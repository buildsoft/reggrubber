﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class MaintenanceNoteHistory
    {
        public int Id { get; set; }
        public int? MaintenanceHistoryId { get; set; }
        public string UserName { get; set; }
        public string Note { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual MaintenanceHistory MaintenanceHistory { get; set; }
    }
}
