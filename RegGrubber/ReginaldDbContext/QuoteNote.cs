﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class QuoteNote
    {
        public int Id { get; set; }
        public int? QuoteId { get; set; }
        public string UserName { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Note { get; set; }
        public string Name { get; set; }

        public virtual Quote Quote { get; set; }
    }
}
