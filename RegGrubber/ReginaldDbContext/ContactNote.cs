﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ContactNote
    {
        public int Id { get; set; }
        public int? ContactId { get; set; }
        public string UserName { get; set; }
        public string Note { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Name { get; set; }

        public virtual Contact Contact { get; set; }
    }
}
