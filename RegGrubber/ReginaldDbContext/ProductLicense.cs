﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ProductLicense
    {
        public ProductLicense()
        {
            CustomerSerial = new HashSet<CustomerSerial>();
        }

        public int Id { get; set; }
        public string Prefix { get; set; }
        public int Flags { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int DaysToExpire { get; set; }
        public string LicenseName { get; set; }
        public int VersionId { get; set; }
        public int EditionId { get; set; }
        public int LicenseTypeId { get; set; }
        public bool GenerateIndividual { get; set; }
        public bool PassCopiesAsLimit { get; set; }
        public int MultipleActivations { get; set; }

        public virtual ProductCatelog Edition { get; set; }
        public virtual ProductVersion Version { get; set; }
        public virtual ICollection<CustomerSerial> CustomerSerial { get; set; }
    }
}
