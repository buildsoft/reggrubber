﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class DongleUpload
    {
        public string DongleNumber { get; set; }
        public string MaxVersion { get; set; }
        public string MaxVersion2 { get; set; }
        public string MaxVersion3 { get; set; }
        public bool? IsValid { get; set; }
        public DateTime? DateExpired { get; set; }
        public int? MaxStarts { get; set; }
    }
}
