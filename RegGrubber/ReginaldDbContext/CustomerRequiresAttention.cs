﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerRequiresAttention
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Reason { get; set; }
        public int IsFixed { get; set; }
        public DateTime DateCreated { get; set; }
        public string FixedBy { get; set; }
        public DateTime? DateFixed { get; set; }
        public string Notes { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
