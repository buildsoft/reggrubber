﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerMaintenance
    {
        public CustomerMaintenance()
        {
            Contracts = new HashSet<Contracts>();
            CustomerMaintenanceProduct = new HashSet<CustomerMaintenanceProduct>();
            CustomerMaintenanceQuote = new HashSet<CustomerMaintenanceQuote>();
            MaintenanceHistory = new HashSet<MaintenanceHistory>();
            MaintenanceNote = new HashSet<MaintenanceNote>();
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime MaintenanceDate { get; set; }
        public decimal MaintenanceAmount { get; set; }
        public string Description { get; set; }
        public int? IsGrandfathered { get; set; }
        public int IsDeleted { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<Contracts> Contracts { get; set; }
        public virtual ICollection<CustomerMaintenanceProduct> CustomerMaintenanceProduct { get; set; }
        public virtual ICollection<CustomerMaintenanceQuote> CustomerMaintenanceQuote { get; set; }
        public virtual ICollection<MaintenanceHistory> MaintenanceHistory { get; set; }
        public virtual ICollection<MaintenanceNote> MaintenanceNote { get; set; }
    }
}
