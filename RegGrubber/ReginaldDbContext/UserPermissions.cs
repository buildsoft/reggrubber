﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class UserPermissions
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string CustomerForm { get; set; }
        public string RegisterUser { get; set; }
        public string ChangePassword { get; set; }

        public virtual User UserNameNavigation { get; set; }
    }
}
