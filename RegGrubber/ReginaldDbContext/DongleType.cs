﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class DongleType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
