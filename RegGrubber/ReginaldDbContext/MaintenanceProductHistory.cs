﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class MaintenanceProductHistory
    {
        public int Id { get; set; }
        public int MaintenanceHistoryId { get; set; }
        public int Quantity { get; set; }
        public int ProductEditionId { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? LicenseType { get; set; }

        public virtual MaintenanceHistory MaintenanceHistory { get; set; }
        public virtual ProductCatelog ProductEdition { get; set; }
    }
}
