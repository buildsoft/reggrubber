﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class LicenseMethod
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ProductLicenseTypeId { get; set; }
        public int? ProductVersionId { get; set; }
        public int LicensingMethod { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DateModified { get; set; }
        public string DefaultTemplate { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductLicenseType ProductLicenseType { get; set; }
        public virtual ProductVersion ProductVersion { get; set; }
    }
}
