﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerType
    {
        public CustomerType()
        {
            Contact = new HashSet<Contact>();
            Customer = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
    }
}
