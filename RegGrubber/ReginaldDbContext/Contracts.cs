﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Contracts
    {
        public Contracts()
        {
            ContractCustomerProductMapping = new HashSet<ContractCustomerProductMapping>();
            CustomerMaintenanceQuote = new HashSet<CustomerMaintenanceQuote>();
            MaintenanceHistory = new HashSet<MaintenanceHistory>();
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public byte ContractType { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public int? ContractLength { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int? BillingPeriod { get; set; }
        public decimal? Price { get; set; }
        public string Note { get; set; }
        public short? State { get; set; }
        public short? BillingType { get; set; }
        public int? MaintenanceId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual CustomerMaintenance Maintenance { get; set; }
        public virtual ICollection<ContractCustomerProductMapping> ContractCustomerProductMapping { get; set; }
        public virtual ICollection<CustomerMaintenanceQuote> CustomerMaintenanceQuote { get; set; }
        public virtual ICollection<MaintenanceHistory> MaintenanceHistory { get; set; }
    }
}
