﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ProductVersion
    {
        public ProductVersion()
        {
            LicenseMethod = new HashSet<LicenseMethod>();
            ProductLicense = new HashSet<ProductLicense>();
        }

        public string VersionName { get; set; }
        public DateTime? BuildDate { get; set; }
        public DateTime ReleaseDate { get; set; }
        public bool? IsCurrent { get; set; }
        public bool? IsReleased { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateCreated { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
        public virtual ICollection<LicenseMethod> LicenseMethod { get; set; }
        public virtual ICollection<ProductLicense> ProductLicense { get; set; }
    }
}
