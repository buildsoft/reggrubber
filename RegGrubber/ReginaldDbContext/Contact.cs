﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Contact
    {
        public Contact()
        {
            ContactNote = new HashSet<ContactNote>();
            Quote = new HashSet<Quote>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? CustomerId { get; set; }
        public int? DistributorId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public int? StatusId { get; set; }
        public int? TypeId { get; set; }
        public string Prefix { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fax { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Distributor Distributor { get; set; }
        public virtual CustomerStatus Status { get; set; }
        public virtual CustomerType Type { get; set; }
        public virtual ICollection<ContactNote> ContactNote { get; set; }
        public virtual ICollection<Quote> Quote { get; set; }
    }
}
