﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerHistoryMapping
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int CustomerUpdateHistoryId { get; set; }
    }
}
