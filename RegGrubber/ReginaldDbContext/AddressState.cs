﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class AddressState
    {
        public AddressState()
        {
            Address = new HashSet<Address>();
        }

        public short Id { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public short? CountryId { get; set; }
        public string SupportLowerBound { get; set; }
        public string SupportUpperBound { get; set; }

        public virtual AddressCountry Country { get; set; }
        public virtual ICollection<Address> Address { get; set; }
    }
}
