﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class Logs
    {
        public int Id { get; set; }
        public DateTime Logged { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Logger { get; set; }
        public string User { get; set; }
        public int? CustomerId { get; set; }
        public string Properties { get; set; }
        public string Callsite { get; set; }
        public string Exception { get; set; }
    }
}
