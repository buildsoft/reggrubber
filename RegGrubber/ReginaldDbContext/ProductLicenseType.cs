﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class ProductLicenseType
    {
        public ProductLicenseType()
        {
            CustomerProduct = new HashSet<CustomerProduct>();
            LicenseMethod = new HashSet<LicenseMethod>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProduct { get; set; }
        public virtual ICollection<LicenseMethod> LicenseMethod { get; set; }
    }
}
