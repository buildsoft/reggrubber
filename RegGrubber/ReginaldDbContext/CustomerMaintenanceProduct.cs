﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerMaintenanceProduct
    {
        public int Id { get; set; }
        public int MaintenanceId { get; set; }
        public int Quantity { get; set; }
        public int ProductEditionId { get; set; }
        public int? LicenseType { get; set; }

        public virtual CustomerMaintenance Maintenance { get; set; }
        public virtual ProductCatelog ProductEdition { get; set; }
    }
}
