﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerMaintenanceQuote
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? MaintenanceId { get; set; }
        public int? QuoteNumber { get; set; }
        public DateTime? DateIssued { get; set; }
        public DateTime? DueDate { get; set; }
        public int? IsManual { get; set; }
        public decimal? Total { get; set; }
        public int? ContractId { get; set; }

        public virtual Contracts Contract { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual CustomerMaintenance Maintenance { get; set; }
    }
}
