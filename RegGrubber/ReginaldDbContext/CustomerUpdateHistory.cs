﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class CustomerUpdateHistory
    {
        public int Id { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string UserName { get; set; }
        public string Reason { get; set; }
        public string Requester { get; set; }
    }
}
