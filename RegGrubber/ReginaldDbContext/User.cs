﻿using System;
using System.Collections.Generic;

namespace RegGrubber.ReginaldDbContext
{
    public partial class User
    {
        public User()
        {
            Customer = new HashSet<Customer>();
            DongleNote = new HashSet<DongleNote>();
            HistoryCustomerName = new HashSet<HistoryCustomerName>();
        }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public bool Status { get; set; }
        public bool ResetPassword { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Role RoleNavigation { get; set; }
        public virtual UserPermissions UserPermissions { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<DongleNote> DongleNote { get; set; }
        public virtual ICollection<HistoryCustomerName> HistoryCustomerName { get; set; }
    }
}
