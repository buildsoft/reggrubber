﻿using RegGrubber.Data;
using RegGrubber.Enums;
using RegGrubber.Extensions;
using RegGrubber.Models;
using RegGrubber.Utils;
using ShellProgressBar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using CustomerMaintenance = RegGrubber.Models.CustomerMaintenance;

namespace RegGrubber
{
    public static class MenuOptions
    {
        public static async Task PrintEnterpriseCubitUsersAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products             = ProductEditionTypeExtensions.GetAllCubitProductTypes(typeof(ProductEditionType));
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, cm => cm.HasCubitEnterpriseFixed || cm.HasCubitEnterpriseFloating);
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "enterprise_cubit_customers");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintManualUpdateCustomersAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products             = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType));
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, c => c.ManualUpdate);
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "customer_maintenance_manual_update");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomerMaintenancesWhichRequreToBeFlagedAsManualUpdateAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products             = ProductEditionTypeExtensions.GetManualUpdateProductsGroup(typeof(ProductEditionType));
                var customerMaintenances = await CustomerMaintenanceRepository.GetCustomersWhoShouldBeMarkedAsManualUpdateAsync(products, (c) => c.InSoftwareAssurance && !c.ManualUpdate && c.CustomerMaintenanceDate > new DateTime(2019, 01, 01));
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "customer_maintenance_require_manual_update_flag");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomerMaintenancesByLicenseGroupsAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 5;
                progressBar.Tick();
                var products             = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType));
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, (c) => c.InSoftwareAssurance && !c.ManualUpdate && c.CustomerMaintenanceDate > new DateTime(2019, 01, 01));
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances.Where(c => c.HasNetworkLicense), "customer_maintenances_networks");
                progressBar.Tick();
                CsvParser.Print(customerMaintenances.Where(c => c.HasTerminalLicense), "customer_maintenances_terminal");
                progressBar.Tick();
                CsvParser.Print(customerMaintenances.Where(c => c.HasStandaloneLicense), "customer_maintenances_standalone");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCountOfCustomersForEachGroupAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 5;
                progressBar.Tick();

                const int bssirl = 16;

                var products             = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType)).ToList();
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, cm => cm.InSoftwareAssurance && !cm.ManualUpdate && cm.CustomerMaintenanceDate > new DateTime(2019, 01, 01));

                progressBar.Tick();
                progressBar.Message    = "Processing...";
                var bssirlCustomers    = customerMaintenances.Where(cm => cm.DistributorId == bssirl).ToList();
                var nonBssirlCustomers = customerMaintenances.Where(cm => cm.DistributorId != bssirl).ToList();

                // BSSIRL
                progressBar.Tick();
                progressBar.Message = "Processing BSSIRL...";

                var cubitStandaloneBssirlCount = bssirlCustomers.Count(cm => cm.HasStandaloneLicense && !cm.HasNetworkLicense && !cm.HasTerminalLicense && cm.HasCubit && !cm.HasGlobal && !cm.HasOffsider && !cm.HasScp);
                var cubitNetworkBssirlCount    = bssirlCustomers.Count(cm => cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasStandaloneLicense && cm.HasCubit && !cm.HasGlobal && !cm.HasOffsider && !cm.HasScp);
                var cubitTerminalBssirlCount   = bssirlCustomers.Count(cm => cm.HasTerminalLicense && !cm.HasNetworkLicense && !cm.HasStandaloneLicense && cm.HasCubit && !cm.HasGlobal && !cm.HasOffsider && !cm.HasScp);

                var eswStandaloneBssirlCount = bssirlCustomers.Count(cm => cm.HasStandaloneLicense && !cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var eswNetworkBssirlCount    = bssirlCustomers.Count(cm => cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasStandaloneLicense && !cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var eswTerminalBssirlCount   = bssirlCustomers.Count(cm => cm.HasTerminalLicense && !cm.HasNetworkLicense && !cm.HasStandaloneLicense && !cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));

                var cubitAndEswStandaloneBssirlCount = bssirlCustomers.Count(cm => cm.HasStandaloneLicense && !cm.HasNetworkLicense && !cm.HasTerminalLicense && cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var cubitAndEswNetworkBssirlCount    = bssirlCustomers.Count(cm => cm.HasNetworkLicense && !cm.HasStandaloneLicense && !cm.HasTerminalLicense && cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var cubitAndEswTerminalBssirlCount   = bssirlCustomers.Count(cm => cm.HasTerminalLicense && !cm.HasStandaloneLicense && !cm.HasNetworkLicense && cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));

                // NON BSSIRL
                progressBar.Tick();
                progressBar.Message = "Processing NON BSSIRL...";

                var cubitStandaloneNonBssirlCount = nonBssirlCustomers.Count(cm => cm.HasStandaloneLicense && !cm.HasNetworkLicense && !cm.HasTerminalLicense && cm.HasCubit && !cm.HasGlobal && !cm.HasOffsider && !cm.HasScp);
                var cubitNetworkNonBssirlCount    = nonBssirlCustomers.Count(cm => cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasStandaloneLicense && cm.HasCubit && !cm.HasGlobal && !cm.HasOffsider && !cm.HasScp);
                var cubitTerminalNonBssirlCount   = nonBssirlCustomers.Count(cm => cm.HasTerminalLicense && !cm.HasNetworkLicense && !cm.HasStandaloneLicense && cm.HasCubit && !cm.HasGlobal && !cm.HasOffsider && !cm.HasScp);

                var eswStandaloneNonBssirlCount = nonBssirlCustomers.Count(cm => cm.HasStandaloneLicense && !cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var eswNetworkNonBssirlCount    = nonBssirlCustomers.Count(cm => cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasStandaloneLicense && !cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var eswTerminalNonBssirlCount   = nonBssirlCustomers.Count(cm => cm.HasTerminalLicense && !cm.HasNetworkLicense && !cm.HasStandaloneLicense && !cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));

                var cubitAndEswStandaloneNonBssirlCount = nonBssirlCustomers.Count(cm => cm.HasStandaloneLicense && !cm.HasNetworkLicense && !cm.HasTerminalLicense && cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var cubitAndEswNetworkNonBssirlCount    = nonBssirlCustomers.Count(cm => cm.HasNetworkLicense && !cm.HasTerminalLicense && !cm.HasStandaloneLicense && cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));
                var cubitAndEswTerminalNonBssirlCount   = nonBssirlCustomers.Count(cm => cm.HasTerminalLicense && !cm.HasNetworkLicense && !cm.HasStandaloneLicense && cm.HasCubit && (cm.HasScp || cm.HasGlobal || cm.HasOffsider));

                var summary = new[]
                {
                    new
                    {
                        CubitStandaloneBssirlCount          = cubitStandaloneBssirlCount,
                        CubitNetworkBssirlCount             = cubitNetworkBssirlCount,
                        CubitTerminalBssirlCount            = cubitTerminalBssirlCount,
                        EswStandaloneBssirlCount            = eswStandaloneBssirlCount,
                        EswNetworkBssirlCount               = eswNetworkBssirlCount,
                        EswTerminalBssirlCount              = eswTerminalBssirlCount,
                        CubitAndEswStandaloneBssirlCount    = cubitAndEswStandaloneBssirlCount,
                        CubitAndEswNetworkBssirlCount       = cubitAndEswNetworkBssirlCount,
                        CubitAndEswTerminalBssirlCount      = cubitAndEswTerminalBssirlCount,
                        CubitStandaloneNonBssirlCount       = cubitStandaloneNonBssirlCount,
                        CubitNetworkNonBssirlCount          = cubitNetworkNonBssirlCount,
                        CubitTerminalNonBssirlCount         = cubitTerminalNonBssirlCount,
                        EswStandaloneNonBssirlCount         = eswStandaloneNonBssirlCount,
                        EswNetworkNonBssirlCount            = eswNetworkNonBssirlCount,
                        EswTerminalNonBssirlCount           = eswTerminalNonBssirlCount,
                        CubitAndEswStandaloneNonBssirlCount = cubitAndEswStandaloneNonBssirlCount,
                        CubitAndEswNetworkNonBssirlCount    = cubitAndEswNetworkNonBssirlCount,
                        CubitAndEswTerminalNonBssirlCount   = cubitAndEswTerminalNonBssirlCount
                    }
                };

                CsvParser.Print(summary, "count_of_customers_by_group");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task ReadCsvBatchReport(ProgressBarOptions options)
        {
            var file = GetFile(GetFiles());

            if (string.IsNullOrEmpty(file))
            {
                Console.WriteLine("File not found");
                return;
            }

            var fileName = Path.GetFileNameWithoutExtension(file);

            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var results     = CsvParser.Read<BatchUpdateRecord>(file).GroupBy(g => g.Id);
                var customersId = results.SelectMany(g => g).Select(g => g.Id).ToList();
                var serials     = await CustomerSerialRepository.GetCustomersWithSerialsAsync(s => customersId.Contains(s.CustomerProduct.CustomerId));
                progressBar.Tick();
                CsvParser.Print(serials.Where(s => s.ProductVersionType == ProductVersionType.Cubit8), $"serials_{fileName}");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }


        public static async Task ReadCsvDeploylxReportAsync(ProgressBarOptions options)
        {
            var file = GetFile(GetFiles());

            if (string.IsNullOrEmpty(file))
            {
                Console.WriteLine("File not found");
                return;
            }

            var fileName = Path.GetFileNameWithoutExtension(file);

            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var results              = CsvParser.Read<DeployLxReport>(file);
                var serials              = results.Select(d => d.SerialNumber).ToList();
                var customers            = await CustomerSerialRepository.GetCustomersWithSerialsAsync(s => serials.Contains(s.Serial));
                var customerIds          = customers.Select(c => c.CustomerId).Distinct().ToList();
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(cm => customerIds.Contains(cm.Id) && !cm.InSoftwareAssurance);
                progressBar.Tick();
                CsvParser.Print(customerMaintenances, $"customers_{fileName}");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task ReadCsvQlmReportAsync(ProgressBarOptions options)
        {
            var file = GetFile(GetFiles());

            if (string.IsNullOrEmpty(file))
            {
                Console.WriteLine("File not found");
                return;
            }

            var fileName = Path.GetFileNameWithoutExtension(file);

            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var results              = CsvParser.Read<QlmReport>(file);
                var serials              = results.Select(d => d.ActivationKey).ToList();
                var customers            = await CustomerSerialRepository.GetCustomersWithSerialsAsync(s => serials.Contains(Regex.Replace(s.Serial, "-", "")));
                var customerIds          = customers.Select(c => c.CustomerId).Distinct().ToList();
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(cm => customerIds.Contains(cm.Id) && !cm.InSoftwareAssurance);
                progressBar.Tick();
                CsvParser.Print(customerMaintenances, $"customers_{fileName}");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintNetworkOrTerminalCustomersAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products             = Enum.GetValues(typeof(ProductEditionType)).Cast<ProductEditionType>();
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, c => c.HasNetworkLicense || c.HasTerminalLicense);
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "customers_with_network_terminal_license");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomersWithCubitEnterpriseFloatingFixedAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var customerMaintenances = await CustomerMaintenanceRepository.GetCustomerMaintenancesAsync(new List<ProductEditionType> {ProductEditionType.CubitEnterpriseFixed, ProductEditionType.CubitEnterpriseFloating});
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "customer_enterprise_fixed");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintAllCustomerMaintenancesAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var customerMaintenances = await CustomerMaintenanceRepository.GetCustomerMaintenancesAsync(ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType)));
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "customer_maintenances");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomersOutOfSoftwareAssuranceAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(c => c.InSoftwareAssurance);
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "customers_out_of_sa");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCubitCustomerOutOfSoftwareAssuranceAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products             = ProductEditionTypeExtensions.GetAllCubitProductTypes(typeof(ProductEditionType));
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, c => !c.InSoftwareAssurance);
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customerMaintenances, "cubit_customers_out_of_sa");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomersWithScpAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 7;
                progressBar.Tick();
                var products = ProductEditionTypeExtensions.GetCubitWithScpProductTypes(typeof(ProductEditionType));

                var customersWithScp = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, c => c.HasScp);
                progressBar.Tick();
                var customersWithScpUnderSa = customersWithScp.Where(c => c.InSoftwareAssurance).ToList();
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customersWithScp, "all_customers_with_scp");
                progressBar.Tick();
                CsvParser.Print(customersWithScpUnderSa, "all_customers_sa_with_scp");
                progressBar.Tick();
                CsvParser.Print(customersWithScpUnderSa.Where(c => c.HasCubit), "customers_sa_with_scp_cubit");
                progressBar.Tick();
                CsvParser.Print(customersWithScp.Where(c => c.HasCubit), "customers_with_scp_cubit");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomersWithGlobalAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 5;
                progressBar.Tick();
                const int bssirl              = 16;
                var       products            = ProductEditionTypeExtensions.GetManualUpdateProductsGroup(typeof(ProductEditionType)).ToList();
                var       customersWithGlobal = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, c => c.InSoftwareAssurance && (!c.DistributorId.HasValue || c.DistributorId != bssirl));
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customersWithGlobal, "customers_with_global");
                progressBar.Tick();
                CsvParser.Print(customersWithGlobal.Where(c => c.HasScp), "customers_with_global_scp");
                progressBar.Tick();
                CsvParser.Print(customersWithGlobal.Where(c => c.HasCubit), "customers_with_global_cubit");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomersWithSerialsAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var serials = await CustomerSerialRepository.GetCustomersWithSerialsAsync();
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(serials, "customers_serials");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintGlobalAndCubitCustomersWithSerialsAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var allProducts = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType));
                var customerMaintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(allProducts, cm => !cm.ManualUpdate && cm.CustomerMaintenanceDate > new DateTime(2018, 11, 13) && (cm.HasCubit || cm.HasGlobal));

                var maintenancesWithCubit    = new List<CustomerMaintenance>();
                var maintenancesWithGlobal   = new List<CustomerMaintenance>();
                var maintenancesWithBtos     = new List<CustomerMaintenance>();
                var maintenancesWithOffsider = new List<CustomerMaintenance>();
                var maintenancesWithScp      = new List<CustomerMaintenance>();

                foreach (var maintenance in customerMaintenances)
                {
                    if (maintenance.HasCubit)
                    {
                        maintenancesWithCubit.Add(maintenance);
                    }

                    if (maintenance.HasGlobal && !maintenance.HasNetworkLicense && !maintenance.HasTerminalLicense)
                    {
                        maintenancesWithGlobal.Add(maintenance);
                    }

                    if (maintenance.HasBtos)
                    {
                        maintenancesWithBtos.Add(maintenance);
                    }

                    if (maintenance.HasOffsider)
                    {
                        maintenancesWithOffsider.Add(maintenance);
                    }

                    if (maintenance.HasScp)
                    {
                        maintenancesWithScp.Add(maintenance);
                    }
                }

                var cubitSerials = await CustomerSerialRepository.GetCustomersWithSerialsAsync(serial => maintenancesWithCubit.Select(m => m.Id).Contains(serial.CustomerProduct.CustomerId) && serial.ProductLicense.VersionId == (int)ProductVersionType.Cubit9);
                var globalSerials = await CustomerSerialRepository.GetCustomersWithSerialsAsync(serial => maintenancesWithGlobal.Select(m => m.Id).Contains(serial.CustomerProduct.CustomerId) && serial.ProductLicense.EditionId == (int)ProductEditionType.Global);

                var groupedCubitSerials = cubitSerials.GroupBy(s => s.CustomerId).ToList();
                var groupedGlobalSerials = globalSerials.GroupBy(s => s.CustomerId).ToList();

                progressBar.Tick();
                progressBar.Message = "Processing...";

                if (groupedCubitSerials.Count != maintenancesWithCubit.Count)
                {
                    var cubitSerialCustomerIds = groupedCubitSerials.Select(s => s.Key);
                    var missingCubitCustomers = maintenancesWithCubit.Where(cm => !cubitSerialCustomerIds.Contains(cm.Id)).ToList();
                    CsvParser.Print(missingCubitCustomers, "missing_cubit_customers");
                }

                if (groupedGlobalSerials.Count != maintenancesWithGlobal.Count)
                {
                    var globalSerialCustomerIds = groupedGlobalSerials.Select(s => s.Key);
                    var missingGlobalCustomers = maintenancesWithGlobal.Where(cm => !globalSerialCustomerIds.Contains(cm.Id)).ToList();
                    CsvParser.Print(missingGlobalCustomers, "missing_global_customers");
                }

                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomerSerialsAsync(ProgressBarOptions options)
        {
            Console.Write("RegId: ");

            if (!int.TryParse(Console.ReadLine(), out var customerId))
            {
                Console.WriteLine("Customer Id should be a number");
                return;
            }

            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var serials = await CustomerSerialRepository.GetCustomersWithSerialsAsync(serial => serial.CustomerProduct.CustomerId == customerId);
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(serials, "customer_serials");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomersWithMistmachProductOrSerialCopiesAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products     = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType));
                var maintenances = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, cm => !cm.ManualUpdate && cm.InSoftwareAssurance && (cm.CubitQuantity > 0 || cm.OffsiderQuantity > 0 || cm.GlobalQuantity > 0));

                var customersId = maintenances.Select(cm => cm.Id);

             //   var serials = await CustomerSerialRepository.GetCustomersWithSerialsAsync(s => s.IsValid.HasValue && s.IsValid > 0 && customersId.Contains(s.CustomerProduct.CustomerId));
                var serials = new List<SerialSummary>();

                var productMistmatches = new List<ProductQuantityMistmatch>();

                var cubitEnterpriseTypes = ProductEditionTypeExtensions.GetEnterpriseCubitProductTypes(typeof(ProductEditionType));

                foreach (var maintenance in maintenances)
                {
                    var cubit9Serials      = serials.Where(s => s.CustomerId == maintenance.Id && s.ProductVersionType == ProductVersionType.Cubit9 && !cubitEnterpriseTypes.Contains(s.ProductEditionType) && s.LicenseType != LicenseType.Network && s.LicenseType != LicenseType.Terminal).ToList();
                    var cubit9FloatingKeys = serials.Where(s => s.CustomerId == maintenance.Id && s.ProductVersionType == ProductVersionType.Cubit9 && (s.LicenseType == LicenseType.Network || s.LicenseType == LicenseType.Terminal)).ToList();
                    var globalSerials      = serials.Where(s => s.CustomerId == maintenance.Id && (int) s.ProductVersionType > 22 && s.ProductEditionType == ProductEditionType.Global && s.LicenseType != LicenseType.Network && s.LicenseType != LicenseType.Terminal).ToList();
                    var offsiderSerials    = serials.Where(s => s.CustomerId == maintenance.Id && (int) s.ProductVersionType > 22 && s.ProductEditionType == ProductEditionType.Offsider && s.LicenseType != LicenseType.Network && s.LicenseType != LicenseType.Terminal).ToList();

                    var maintenanceCubitQuantity = maintenance.CubitQuantity == maintenance.CubitEnterpriseQuantity ? maintenance.CubitQuantity : maintenance.CubitQuantity - maintenance.CubitEnterpriseQuantity;
                    var serialsCubitQuantity     = cubit9Serials.Count + cubit9FloatingKeys.Select(s => s.Copies).Sum();

                    var productSerialDifference = new ProductQuantityMistmatch
                    {
                        CustomerId                        = maintenance.Id,
                        CubitSerialsQuantityDifference    = maintenanceCubitQuantity - serialsCubitQuantity,
                        GlobalSerialsQuantityDifference   = maintenance.HasNetworkLicense || maintenance.HasTerminalLicense ? 0 : maintenance.GlobalQuantity - globalSerials.Count,
                        OffsiderSerialsQuantityDifference = maintenance.HasNetworkLicense || maintenance.HasTerminalLicense ? 0 : maintenance.OffsiderQuantity - offsiderSerials.Count
                    };

                    productMistmatches.Add(productSerialDifference);
                }

                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(productMistmatches.Where(p => p.CubitSerialsQuantityDifference != 0 || p.GlobalSerialsQuantityDifference != 0 || p.OffsiderSerialsQuantityDifference != 0), "customers_quantity_mistmatch");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintCustomerWithProfessionalFixedNetworkTerminal(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products = new [] { ProductEditionType.CubitProfessionalFixed };
                var customers = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, cm => cm.InSoftwareAssurance && (cm.HasNetworkLicense || cm.HasTerminalLicense));
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customers, "customer_professional_fixed_network_terminal");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintLicenseInfoAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 5;
                progressBar.Tick();

                var licenseKeysXml = XElement.Load(Path.Combine(@"C:\qlm", "LicenseKeys.xml"));
                var customersXml   = XElement.Load(Path.Combine(@"C:\qlm", "Customers.xml"));

                var qlmCustomers = (from table in customersXml.Elements("Table")
                    select new QlmCustomerTable
                    {
                        Id         = int.Parse(table.Element("ID")?.Value),
                        CustomerId = int.Parse(table.Element("CustomerID")?.Value),
                        Email      = table.Element("Email")?.Value,
                    }).Where(c => !c.Email.Contains("buildsoft")).ToList();

                var qlmLicenseKeys = (from table in licenseKeysXml.Elements("Table")
                    select new QlmLicenseTable
                    {
                        Id                = int.Parse(table.Element("ID")?.Value),
                        ActivationCount   = int.Parse(table.Element("ActivationCount")?.Value),
                        ActivationKey     = table.Element("ActivationKey")?.Value,
                        UserId            = int.Parse(table.Element("UserID")?.Value),
                        ProductId         = int.Parse(table.Element("ProductID")?.Value),
                        MajorVersion      = table.Element("MajorVersion")?.Value,
                        MinorVersion      = table.Element("MinorVersion")?.Value,
                        NumLicenses       = int.Parse(table.Element("NumLicenses")?.Value),
                        AvailableLicenses = int.Parse(table.Element("AvailableLicenses")?.Value),
                        FloatingSeats     = int.Parse(table.Element("FloatingSeats")?.Value),
                        Features          = table.Element("Features")?.Value

                    }).Where(l => l.UserId > 0).ToList();
                
                foreach (var licenseKey in qlmLicenseKeys)
                {
                    var customer = qlmCustomers.FirstOrDefault(c => c.CustomerId == licenseKey.UserId);

                    if (customer == null)
                        continue;

                    licenseKey.Email = customer.Email;
                    licenseKey.Evaluation = licenseKey.Features.Contains("16");
                    licenseKey.Student = licenseKey.Features.Contains("32");
                }

                progressBar.Tick();

                var deployLxCubitVersions = new List<ProductVersionType>
                {
                    ProductVersionType.Cubit4,
                    ProductVersionType.Cubit5,
                    ProductVersionType.Cubit6
                };

                var qlmCubitVersions = new List<ProductVersionType>
                {
                    ProductVersionType.Cubit7,
                    ProductVersionType.Cubit8,
                    ProductVersionType.Cubit9
                };

                var nonCommCubitVersions = new List<ProductEditionType>
                {
                    ProductEditionType.CubitEvaluation,
                    ProductEditionType.CubitStudent,
                    ProductEditionType.Student
                };

                var cubitEditions = ProductEditionTypeExtensions.GetAllCubitProductTypes(typeof(ProductEditionType)).ToList();
                var customers     = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(cubitEditions, cm => cm.InSoftwareAssurance);
                var serials       = await CustomerSerialRepository.GetCustomersWithSerialsAsync(s => (qlmCubitVersions.Contains((ProductVersionType) s.ProductLicense.VersionId) || deployLxCubitVersions.Contains((ProductVersionType) s.ProductLicense.VersionId)) &&
                                                                                                     !s.CustomerProduct.Customer.Email.Contains("buildsoft", StringComparison.OrdinalIgnoreCase)
                                                                                                     && !s.CustomerProduct.Customer.Name.Contains("test", StringComparison.OrdinalIgnoreCase)
                                                                                                     && !s.CustomerProduct.Customer.Name.Contains("krishna", StringComparison.OrdinalIgnoreCase)
                                                                                                     && !s.CustomerProduct.Customer.Name.Contains("rizwana", StringComparison.OrdinalIgnoreCase)
                                                                                                     && !s.CustomerProduct.Customer.Name.Contains("buildsoft", StringComparison.OrdinalIgnoreCase));

                progressBar.Tick();

                foreach (var key in qlmLicenseKeys)
                {
                    if (string.IsNullOrWhiteSpace(key.ActivationKey))
                        continue;

                    var serialSummary = new SerialSummary
                    {
                        Copies = key.FloatingSeats == 0 ? key.NumLicenses : key.FloatingSeats,
                        Email  = key.Email,

                    };

                    if (key.Student)
                    {
                        serialSummary.ProductEditionType = ProductEditionType.CubitStudent;
                    }
                    else if (key.Evaluation)
                    {
                        serialSummary.ProductEditionType = ProductEditionType.CubitEvaluation;
                    }

                    if (key.MajorVersion.Equals("7"))
                    {
                        serialSummary.ProductVersionType = ProductVersionType.Cubit7;
                    }
                    else if (key.MajorVersion.Equals("8"))
                    {
                        serialSummary.ProductVersionType = ProductVersionType.Cubit8;

                    }
                    else if (key.MajorVersion.Equals("9"))
                    {
                        serialSummary.ProductVersionType = ProductVersionType.Cubit9;

                    }

                    serials.Add(serialSummary);
                }

                progressBar.Tick();

                var customerIds = customers.Select(c => c.Id).ToList();

                var serialsWithSoftwareAssurance = serials.Where(s => customerIds.Contains(s.CustomerId)).ToList();

                var totalOfCopies           = 0;
                var totalOfCommercialCopies = 0;

                var totalOfCubit4Copies = 0;
                var totalOfCubit5Copies = 0;
                var totalOfCubit6Copies = 0;

                var totalOfCubit7Copies = 0;
                var totalOfCubit8Copies = 0;
                var totalOfCubit9Copies = 0;

                foreach (var serial in serials.Where(s => cubitEditions.Contains(s.ProductEditionType)))
                {
                    totalOfCopies += serial.Copies;

                    if (nonCommCubitVersions.Contains(serial.ProductEditionType))
                        continue;

                    totalOfCommercialCopies += serial.Copies;

                    switch (serial.ProductVersionType)
                    {
                        case ProductVersionType.Cubit4:
                            totalOfCubit4Copies += serial.Copies;
                            break;
                        case ProductVersionType.Cubit5:
                            totalOfCubit5Copies += serial.Copies;
                            break;
                        case ProductVersionType.Cubit6:
                            totalOfCubit6Copies += serial.Copies;
                            break;

                        case ProductVersionType.Cubit7:
                            totalOfCubit7Copies += serial.Copies;
                            break;
                        case ProductVersionType.Cubit8:
                            totalOfCubit8Copies += serial.Copies;
                            break;
                        case ProductVersionType.Cubit9:
                            totalOfCubit9Copies += serial.Copies;
                            break;
                    }
                }

                var totalOfCopiesWithSoftwareAssurance           = 0;
                var totalOfCommercialCopiesWithSoftwareAssurance = 0;

                var totalOfCubit4CopiesWithSoftwareAssurance = 0;
                var totalOfCubit5CopiesWithSoftwareAssurance = 0;
                var totalOfCubit6CopiesWithSoftwareAssurance = 0;

                var totalOfCubit7CopiesWithSoftwareAssurance = 0;
                var totalOfCubit8CopiesWithSoftwareAssurance = 0;
                var totalOfCubit9CopiesWithSoftwareAssurance = 0;

                foreach (var serial in serialsWithSoftwareAssurance.Where(s => cubitEditions.Contains(s.ProductEditionType)))
                {
                    totalOfCopiesWithSoftwareAssurance += serial.Copies;

                    if (nonCommCubitVersions.Contains(serial.ProductEditionType))
                        continue;

                    totalOfCommercialCopiesWithSoftwareAssurance += serial.Copies;

                    switch (serial.ProductVersionType)
                    {
                        case ProductVersionType.Cubit4:
                            totalOfCubit4CopiesWithSoftwareAssurance += serial.Copies;
                            break;
                        case ProductVersionType.Cubit5:
                            totalOfCubit5CopiesWithSoftwareAssurance += serial.Copies;
                            break;
                        case ProductVersionType.Cubit6:
                            totalOfCubit6CopiesWithSoftwareAssurance += serial.Copies;
                            break;
                        case ProductVersionType.Cubit7:
                            totalOfCubit7CopiesWithSoftwareAssurance += serial.Copies;
                            break;
                        case ProductVersionType.Cubit8:
                            totalOfCubit8CopiesWithSoftwareAssurance += serial.Copies;
                            break;
                        case ProductVersionType.Cubit9:
                            totalOfCubit9CopiesWithSoftwareAssurance += serial.Copies;
                            break;
                    }
                }

                var copies = new List<dynamic>
                {
                    new {Description = "Total Of Commercial Cubit 4 Copies", Total = totalOfCubit4Copies},
                    new {Description = "Total Of Commercial Cubit 5 Copies", Total = totalOfCubit5Copies},
                    new {Description = "Total Of Commercial Cubit 6 Copies", Total = totalOfCubit6Copies},
                    new {Description = "Total Of Commercial Cubit 7 Copies", Total = totalOfCubit7Copies},
                    new {Description = "Total Of Commercial Cubit 8 Copies", Total = totalOfCubit8Copies},
                    new {Description = "Total Of Commercial Cubit 9 Copies", Total = totalOfCubit9Copies},
                    new {Description = "Total Of Copies", Total                    = totalOfCopies},
                    new {Description = "Total Of Commercial Copies", Total         = totalOfCommercialCopies}
                };

                var copiesWithSa = new List<dynamic>
                {
                    new {Description = "Total Of Commercial Cubit 4 Copies SA", Total = totalOfCubit4CopiesWithSoftwareAssurance},
                    new {Description = "Total Of Commercial Cubit 5 Copies SA", Total = totalOfCubit5CopiesWithSoftwareAssurance},
                    new {Description = "Total Of Commercial Cubit 6 Copies SA", Total = totalOfCubit6CopiesWithSoftwareAssurance},
                    new {Description = "Total Of Commercial Cubit 7 Copies SA", Total = totalOfCubit7CopiesWithSoftwareAssurance},
                    new {Description = "Total Of Commercial Cubit 8 Copies SA", Total = totalOfCubit8CopiesWithSoftwareAssurance},
                    new {Description = "Total Of Commercial Cubit 9 Copies SA", Total = totalOfCubit9CopiesWithSoftwareAssurance},
                    new {Description = "Total Of Copies SA", Total                    = totalOfCopiesWithSoftwareAssurance},
                    new {Description = "Total Of Commercial Copies SA", Total         = totalOfCommercialCopiesWithSoftwareAssurance}
                };

                

                CsvParser.Print(copies, "total_of_copies");
                CsvParser.Print(copiesWithSa, "total_of_copies_with_sa");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        public static async Task PrintAllCustomersSummaryAsync(ProgressBarOptions options)
        {
            using (var progressBar = new ProgressBar(0, "Querying...", options))
            {
                progressBar.MaxTicks = 3;
                progressBar.Tick();
                var products  = ProductEditionTypeExtensions.GetAllProducts(typeof(ProductEditionType));
                var customers = await CustomerMaintenanceRepository.GetGroupedCustomerMaintenancesAsync(products, cm => !string.IsNullOrWhiteSpace(cm.Company));
                progressBar.Tick();
                progressBar.Message = "Processing...";
                CsvParser.Print(customers.OrderBy(c => c.DistributorId), "customers_summary");
                progressBar.Tick();
                progressBar.Message = "Complete";
            }
        }

        private static string[] GetFiles()
        {
            Console.WriteLine();
            Console.Write("Folder path: ");

            var directoryPath = Console.ReadLine();

            if (!Directory.Exists(directoryPath))
            {
                Console.WriteLine("Invalid Folder path");
                return null;
            }

            var files = Directory.GetFiles(directoryPath);

            return files;
        }

        private static string GetFile(params string[] files)
        {
            if (files == null || files.Length == 0)
                return null;

            for (var i = 0; i < files.Length; i++)
            {
                Console.WriteLine($"{i + 1}: {Path.GetFileName(files[i])}");
            }

            Console.Write("Index of File: ");

            if (!int.TryParse(Console.ReadLine(), out var fileIndex))
                return null;

            fileIndex -= 1;

            if (fileIndex <= files.Length && fileIndex >= 0)
                return files[fileIndex];

            return null;
        }
    }
}