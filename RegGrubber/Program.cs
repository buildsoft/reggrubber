﻿using System;
using System.Threading.Tasks;

namespace RegGrubber
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            try
            {
                await MainMenu.BuildMenu();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Press Enter to close...");
                Console.Read();
            }
        }
    }
}